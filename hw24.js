let display = document.getElementById('display');
let ans = document.getElementById("ans");
let firstNumber = 0;
let secondNumber = 0;
let operation = '';
let operationPressed = false;
let memory = 0;
let memoryFlag = false;
let counter = 0;
let counter1 = 0;
let btn;
function memPlus() {
    operation = 'm+';
    memory = +ans.value;
    ans.value = '';
    memoryFlag = true;
    counter++;
}
function memMin() {
    operation = 'm-';
    memory = -(+ans.value);
    ans.value = '';
    memoryFlag = true;
    counter++;
}
function mrc() {
    if (counter1 === 0) {
        ans.value = memory;
        counter1++;
    } else if (counter1 === 1) {
        btn.remove();
        ans.value = '';
        ans.style.width = '220px';
        ans.style.top = '2px';
        ans.style.left = '2px';
        ans.style.borderLeft = "none";
        counter = 0;
        counter1 = 0;
    }
}

function division() {
    operation = '/';
    operationPressed = true;
    firstNumber = +ans.value;
}
function plus() {
    operation = '+';
    operationPressed = true;
    firstNumber = +ans.value;
}
function multp() {
    operation = '*';
    operationPressed = true;
    firstNumber = +ans.value;
}
function minus() {
    operation = '-';
    operationPressed = true;
    firstNumber = +ans.value;
}

function equals() {
    secondNumber = +ans.value;
    if (operation == "+") {
        let result = firstNumber + secondNumber;
        ans.value = result;
    }
    if (operation == "-") {
        let result = firstNumber - secondNumber;
        ans.value = result;
    }
    if (operation == "*") {
        let result = firstNumber * secondNumber;
        ans.value = result;
    }
    if (operation == "/") {
        let result = firstNumber / secondNumber;
        ans.value = result;
    }
    if (operation == "m+") {
        ans.value = memory;
    }
    if (operation == "m-") {
        ans.value = memory;
    }


}
keys.onclick = function (event) {
    let digit = parseInt(event.target.value);

    if (!isNaN(digit)) {
        if (operationPressed) {
            ans.value = digit;
            operationPressed = false;

        }
        else if (ans.value == 0) {
            ans.value = digit;
        }
        else {
            ans.value += digit;
        }
    } else if (event.target.value === 'C') {
        ans.value = '';
    }
    if (event.target.value === 'm+' || event.target.value === 'm-' ) {
        if (counter === 1 && memoryFlag) {
            btn = document.createElement('button');
            btn.innerHTML = 'm';
            ans.style.width = '200px';
            ans.style.top = '-38px';
            ans.style.left = '20px';
            ans.style.borderLeft = "none";
            display.insertBefore(btn, display.firstChild);
            memoryFlag = false;
        }

    }
};